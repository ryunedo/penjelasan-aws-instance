# Belajar Amazon Web Services
## _Belajar Menggunakan AWS EC2_


### Amazon Elastic Compute Cloud (EC2)
>Amazon Elastic Compute Cloud (Amazon EC2) adalah layanan web yang menyediakan kapasitas komputasi yang aman dan berukuran fleksibel di cloud. Amazon EC2 dirancang untuk membuat komputasi cloud berskala web lebih mudah bagi para pengembang.

## Overview Instance
| Launch an instance | Type |
| ------ | ------ |
| Name And Tag | ryann |
| Application and OS Images (Amazon Machine Image) | Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type |
| Instance type | t2.micro |
| Key pair | unedoo.ppk |
| Network settings(Security Group) | launch-wizard-3 |
| Configure storage | Default  |
| Advanced details | Default (Kecuali User data) |


# Penjelasan Lebih Lanjut
## name tag : ryann 

## Application an OS Image (Amazon Machine Image) 
> Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
>Dipilih Karena Free tier eligible 

## Instance Type
> t2.micro
>Dipilih karena Frree eligible
>dan memiliki spesifikasi : -1CPU dan 1GB Memory


## Key Pair
> Dibuat untuk lebih mengamankan instance yang kita buat dengan key pair


## Network settings
|   |   |
| ------ | ------ |
| VPC | Default |
|Subnet | No Preference |
| Auto-assign public IP | Enable |
| Firewall ( Security Group) | launch-wizard-3 |
| Security Group Name | launch-wizard-3 |
> Security Group launch wizard 3 Dengan Spesifikasi : 
> - Allow ssh traffic from anywhere 
>- Allow https traffic from internet
> - Allow http traffic from internet

> di allow semua karena hanya buat awalan belajar, agar semua orang dapat mengakses dari http atau ssh

## configure stroage
> default = storage 1x8gb dan root volume general purpose ssd (gp2)

## Advanced detail
> Semua default kecuali "user data"

## User Data pada Advanced Detail : 
```sh
#!/bin/bash
yum -y install httpd
systemctl enable httpd
systemctl start httpd
echo '<html><h2>Hello World</h2></html>'
 > /var/www/html/index.html
```
Perintah yum sendiri merupakan utilitas yang dapat digunakan untuk menginstall, menghapus, atau mencari package.
-y merupakan keterangan untuk yes pada bagian install httpd atau apache.
```ssh
yum -y install httpd
```

Perintah dibawah merupakah perintah yang merujuk agar apache dapat dinyalakan pada webserver
```ssh
systemctl enable httpd 
```

Perintah dibawah berfungsi untuk memulai apache pada webserver
```ssh
systemctl start httpd 
```

Perintah dibawah merupakah perintah echo atau menampilkan text ke layar
```ssh
echo '<html><h1>Hello World</h1></html>'
```

perintah ">" dibawah berfungsi sebagai arahan pada dimana apache berada
```ssh
 > /var/www/html/index.html
 ```
 
## Credit
### Febryano Unedo Lbn Siantar